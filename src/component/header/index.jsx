import React, { useState, useContext } from "react";
import { MyBox, Image, MyLink } from "./styled-component";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Grid from "@mui/material/Grid";
import { Typography, Box } from "@mui/material";
import Blowatch from "./../../asset/image/bloowatch.png";
import { Link } from "react-router-dom";
import "./style.css";
import { DataContext } from "../../contexts/contextData";

const Index = () => {
  const { cart, navbarItems } = useContext(DataContext);
  console.log("navbar data  cart lenght", cart);
  return (
    <>
      <Grid container>
        <Grid item md={8} sm={4} xs={3}>
          <Link to="/">
            <Image className="logo-image" src={Blowatch} alt="logo image" />
          </Link>
        </Grid>
        <Grid item md={4} sm={8} xs={9}>
          <Box
            style={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              marginTop: "20px",
            }}
          >
            {navbarItems.map((obj) => (
              <Link
                style={{
                  textDecoration: "none",
                  color: "black",
                }}
                to={obj.path}
              >
                <Typography style={{ fontSize: "14px", fontWeight: "600" }}>
                  {obj.name}
                </Typography>
              </Link>
            ))}
            <Box style={{ display: "flex" }}>
              <Link
                style={{
                  textDecoration: "none",
                  color: "black",
                }}
                to="/cart"
              >
                <Typography style={{ fontSize: "14px", fontWeight: "600" }}>
                  CART
                </Typography>
              </Link>
              <ShoppingCartIcon style={{ color: "black" }} />
              <Box
                sx={{
                  height: "20px",
                  width: "20px",
                  background: "blue",
                  borderRadius: "50%",
                  transform: "translate(-22px,-18px)",
                  color: "white",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <span style={{ fontSize: "10px" }}>{cart?.length}</span>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </>
  );
};

export default Index;
