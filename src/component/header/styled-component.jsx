import styled from "styled-components";
export const MyBox = styled.div`
  margin-top: 20px;
`;

export const Image = styled.img`
  width: 10%;
  float: left;
  margin-bottom: 10px;
  padding-left: 20px;
`;
export const MyLink = styled.a`
  .navbar-link {
    color: black;
    font-size: 15px;
    padding-left: 30px;
    font-weight: 600;
    text-decoration: none;
    margin-top: 20px;
  }
`;
