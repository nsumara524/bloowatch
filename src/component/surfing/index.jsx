import React, { useState, useContext, useEffect } from "react";
import "./style.css";
import {
  MyBox,
  CustomBox,
  ButtonGroup,
  Paragraph,
  Lable,
  SearchBox,
  SmallBox,
} from "./styled-component";
import { Grid, Typography, Button, Box, Rating } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { Paper, InputBase, IconButton, Slider, Container } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { DataContext } from "../../contexts/contextData";
const Surfing = () => {
  const { cart, setCart, productssArray, relProduct, categories } =
    useContext(DataContext);
  console.log("hlo data from cart ", cart);
  const [productsArray, setProductArray] = useState([...productssArray]);
  const [copyproductArray, setCopyProductArray] = useState([...productssArray]);
  const [sliderArray, setSliderArray] = useState([0, 100]);
  const navigate = useNavigate();
  const [highlightcategories, sethighlightcategories] = useState();
  const addProduct = (obj) => {
    setCart((cart) => [...cart, obj]);
    console.log("cartItem ", cart);
  };
  const removeClick = (id) => {
    console.log("Delete  from surfing cart ", id);
    const filtered = cart.filter((data, index) => data !== id);
    setCart(filtered);
  };
  function handleSearch(event) {
    let searchData = productsArray.filter((obj) =>
      obj.para.toLowerCase().includes(event.target.value.toLowerCase())
    );
    setProductArray(searchData);
  }
  function handleprice(event) {
    console.log("price change", event.target.value);

    let searchData = copyproductArray.filter(
      (obj) =>
        obj.price > event.target.value[0] && obj.price < event.target.value[1]
    );
    console.log("searchData", searchData);
    setProductArray(searchData);
  }
  function detailProduct(obj) {
    console.log("veiw product dettail", obj);
    navigate("/viewProduct", { state: obj });
  }
  useEffect(() => {
    localStorage.setItem("localStorage", JSON.stringify(cart));
  }, [cart]);
  function handleCategories(objone) {
    sethighlightcategories(objone);
    let searchData = copyproductArray.filter((obj) =>
      obj.categories.toLowerCase().includes(objone.name.toLowerCase())
    );
    console.log("searchData", searchData);
    setProductArray(searchData);
  }
  return (
    <>
      <MyBox>SURFING</MyBox>
      <Container>
        <Grid container>
          <Grid container item md={9} sm={12}>
            {productsArray.map((obj) => (
              <Grid item md={4} sm={6} xs={12}>
                <>
                  <Paragraph className="sale-position">
                    <Typography className="sale-title" variant="h6">
                      {obj.sale}
                    </Typography>
                  </Paragraph>
                  <Box
                    sx={{
                      height: "350px !important",
                      // width: "309px",
                      display: "flex",
                      marginTop: "30px",
                      justifyContent: "center",
                    }}
                  >
                    <img
                      className="image-hover"
                      src={obj.image}
                      alt="womenn wet suit"
                      onClick={() => detailProduct(obj)}
                      style={{
                        maxWidth: "100%",
                        maxHeight: "100%",
                      }}
                    />
                  </Box>
                  <CustomBox>
                    {cart.includes(obj) ? (
                      <Button className="btns" onClick={() => removeClick(obj)}>
                        {obj.remove}
                      </Button>
                    ) : (
                      <Button className="btns" onClick={() => addProduct(obj)}>
                        {obj.button}
                      </Button>
                    )}

                    <Typography className="lighttwave-board" variant="h5">
                      {obj.para}
                    </Typography>
                    <Typography className="equipment-link" variant="h6">
                      {obj.equipment}
                    </Typography>
                    <ButtonGroup
                      style={{ justifyContent: "center", display: "flex" }}
                    >
                      <Button className="price-btn">{obj.price}</Button>
                      <Button className="price-button">{obj.price_btn}</Button>
                    </ButtonGroup>
                  </CustomBox>
                </>
              </Grid>
            ))}
          </Grid>
          <Grid className="related-product" item md={3} sm={12}>
            <Lable>
              <Paper
                component="form"
                sx={{
                  marginTop: "150px",
                  display: "flex",
                  textAlign: "left",
                  width: "300px",
                }}
              >
                <IconButton sx={{ p: "10px" }} aria-label="menu"></IconButton>
                <InputBase
                  placeholder="Search "
                  inputProps={{ "aria-label": "search google maps" }}
                  onChange={handleSearch}
                />
                <IconButton
                  type="button"
                  sx={{ p: "10px" }}
                  aria-label="search"
                >
                  <SearchIcon />
                </IconButton>
              </Paper>

              <Typography className="price"> Price </Typography>
              <Box width={300}>
                <Slider
                  size="small"
                  defaultValue={sliderArray}
                  aria-label="Small"
                  valueLabelDisplay="auto"
                  onChange={handleprice}
                  step={10}
                  marks
                  min={0}
                  max={1000}
                />
              </Box>
              <Typography className="price-rang"> Price:$0 - $100 </Typography>
              <SearchBox>
                <Typography className="categories">CATEGORIES</Typography>
                {categories.map((obj) => (
                  <Typography
                    onClick={() => handleCategories(obj)}
                    className={
                      obj?.name?.toLowerCase() ===
                      highlightcategories?.name.toLowerCase()
                        ? "board1"
                        : "board"
                    }
                  >
                    {obj.name}
                  </Typography>
                ))}
              </SearchBox>
            </Lable>
            <Box>
              <SmallBox>
                <Typography className="related-product">
                  RELATED PRODUCT
                </Typography>
                {relProduct.map((obj) => (
                  <div
                    style={{
                      display: "flex",
                      // paddingLeft: "auto",
                      // paddingRight: "auto",
                    }}
                  >
                    <div className="related-product-image">
                      <img
                        src={obj.smallBoard}
                        style={{
                          float: "left",
                        }}
                        alt="small board"
                      />
                    </div>
                    <div>
                      <Typography className="related-product-title">
                        {obj.title}
                      </Typography>
                      <Rating
                        defaultValue={obj.rating}
                        precision={0.5}
                        readOnly
                      />
                    </div>
                  </div>
                ))}
              </SmallBox>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default Surfing;
