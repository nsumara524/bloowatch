import styled from "styled-components";
export const MyBox = styled.div`
  background: darkblue;
  color: white;
  font-size: 30px;
  padding-left: 120px;
  max-width: 100%;
  text-align: left;
  padding-top: 70px;
  height: 100px;
`;

export const CustomBox = styled.div`
  .btns {
    background: darkblue;
    font-size: 15px;
    color: white;
    margin-top: 17px;
    conten: center;
    padding: 8px 51px 8px 51px;
  }
  .btns:hover {
    background: darkblue !important;
  }
  .equipment-link {
    font-size: 15px;
    color: blue;
  }
  .lighttwave-board {
    margin-top: 32px;
    font-size: 15px;
    font-weight: 600;
  }
  .lighttwave {
    font-size: 15px;
    font-weight: 600;
    margin-top: -60px;
  }
`;

export const ButtonGroup = styled.div`
  hover: {
    "&:hover": {
      background: darkblue;
      font-size: 11px;
      color: white;
      margin-top: 37px;
    }
  }
  display-flex: center;
  .price-btn {
    background: darkblue;
    font-size: 11px;
    color: white;
    margin-top: 37px;
  }
  .price-btn:hover {
    background: darkblue;
  }

  .price-button {
    padding-top: 39px;
    font-size: 11px;
    text-decoration: line-through;
    color: black;
  }
`;
export const Paragraph = styled.p`
  color: white;
  background-color: darkblue;
  padding: 15px;
  width: 20px;
  height: 20px;
  text-align: center;
  border-radius: 500px;
  transform: translate(35px, 62px);
  .sale-title {
    font-size: 12px;
  }
`;

export const Lable = styled.div`
  .price {
    font-size: 20px;
    margin-top: 40px;
    font-weight: 600;
    text-align: left;
  }
  .price-rang {
    font-size: 20px;
    margin-top: 40px;
    font-weight: 600;
    text-align: left;
  }
`;
export const SearchBox = styled.div`
  .categories {
    font-size: 20px;
    text-align: left;
    font-weight: 600;
    margin-top: 40px;
  }
  .board {
    color: gray;
    font-size: 20px;g
    font-weight: 500;
    text-align: left;
    margin-top: 15px;
    cursor:pointer
  }
  .board1 {
    color: black;
    font-size: 20px;
    font-weight: 600;
    text-align: left;
    margin-top: 15px;
    cursor:pointer
  }
`;
export const SmallBox = styled.div`
  .related-product {
    text-align: left;
    margin-top: 55px;
    font-size: 20px;
    font-weight: 600;
  }
  .related-product-image {
    width: 100px;
    height: 130px;
    display: flex;
    float: left;
    margin-top: 40px;
    background: #fafafa;
  }
  .related-product-title {
    margin-top: 80px;
  }
`;
