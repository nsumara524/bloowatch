import React, { useContext } from "react";
import { Grid, Typography, Box } from "@mui/material";
import { FooterBox } from "./styled-component";
import { DataContext } from "../../contexts/contextData";
import { Container } from "@mui/system";
const Footer = () => {
  const { instGram, FooterAraydata } = useContext(DataContext);

  return (
    <>
      <FooterBox className="footer">
        <Container>
          <Grid container>
            {FooterAraydata?.map((obj, index) => (
              <Grid Item md={2} sm={4} xs={12}>
                <Typography className="about">{obj.heading}</Typography>
                {obj?.paragraph?.map((item, index) => (
                  <Typography className="paragraph" variant="h6">
                    {item}
                  </Typography>
                ))}
              </Grid>
            ))}

            <Grid item md={3} xs={12}>
              <Typography variant="h6" className="about">
                INSTAGRAM
              </Typography>
              <Box style={{ display: "flex" }}>
                {instGram.map((obj) => (
                  <img
                    className="footer-image"
                    src={obj.image}
                    alt="small board"
                  />
                ))}
              </Box>
            </Grid>
          </Grid>
        </Container>
      </FooterBox>
    </>
  );
};

export default Footer;
