import styled from "styled-components";
export const FooterBox = styled.div`
  background: black;
  text-align: left;
  margin-top: 60px;
  color: white;
  padding-top: 70px;
  padding-bottom: 30px;

  .paragraph {
    margin-top: 30px;
    font-size: 13px;
    padding-right: 25px;
  }
  .paragraph-email {
    margin-top: 10px;
    font-size: 12px;
  }

  .footer-image {
    height: 30px;
    padding-right: 5px;
    float: right;
  }
`;
