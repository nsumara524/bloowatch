import React, { useState } from "react";
import "./App.css";
import Dashboard from "./pages/dashboard/index";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Cart from "./pages/cart/index";
import Blog from "./pages/blog/index";
import Productdetail from "./pages/viewDetail/index";
import {
  mainArray,
  relatedProduct,
  objCategories,
  instaGram,
  navItems,
  billTotoal,
  tablecells,
  textSlid,
  detailArray,
  footerData,
} from "./data";
import { DataContext } from "./contexts/contextData";

function App() {
  const [cart, setCart] = useState([]);
  const [productssArray, setProductArray] = useState([...mainArray]);
  const [relProduct, setRelatedProduct] = useState([...relatedProduct]);
  const [categories, setCategories] = useState([...objCategories]);
  const [instGram, seInstaGram] = useState([...instaGram]);
  const [navbarItems, seNavItems] = useState([...navItems]);
  const [billcartTotoal, setBillTotoal] = useState([...billTotoal]);
  const [tablecellsdata, setTablecells] = useState([...tablecells]);
  const [texttitleSlid, settextSlid] = useState([...textSlid]);
  const [detailofArray, setDetailArray] = useState([...detailArray]);
  const [FooterAraydata, setFooterAllData] = useState([...footerData]);
  return (
    <div className="App">
      <DataContext.Provider
        value={{
          cart,
          setCart,
          productssArray,
          relProduct,
          categories,
          instGram,
          navbarItems,
          billcartTotoal,
          tablecellsdata,
          texttitleSlid,
          detailArray,
          detailofArray,
          FooterAraydata,
        }}
      >
        <Router>
          <Routes>
            <Route path="/" element={<Dashboard />}></Route>
            <Route path="/cart" element={<Cart />}></Route>
            <Route path="/viewProduct" element={<Productdetail />}></Route>
            <Route path="/blog" element={<Blog />}></Route>
          </Routes>
        </Router>
      </DataContext.Provider>
    </div>
  );
}

export default App;
