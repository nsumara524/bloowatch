import React, { createContext, useState } from "react";
// import LightBoard from "./../../asset/image/lightwave-board.png";
import LightBoard from "./asset/image/lightwave-board.png";
import Men from "./asset/image/men.png";
import Women from "./asset/image/women.png";
import paddleonly from "./asset/image/images.png";
import Kayak from "./asset/image/kayak.png";
import paddle from "./asset/image/paddal.png";
import sliderone from "./asset/image/powersite.png";
import slidertwo from "./asset/image/sidetwo.jpg";
import sliderthree from "./asset/image/water water.jpg";
import sliderfour from "./asset/image/watersite.jpg";
export const GlobalData = createContext();

export const mainArray = [
  {
    id: 1,
    qty: 1,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis.",
    sale: "sale",
    image: LightBoard,
    sku: "003",
    categories: "board",
    tags: "Board,Surf,Surifing",
    remove: "Remove",
    button: "ADD TO CART",
    para: "LIGHTWAVE BOARD",
    equipment: "Equipment,Board",
    price: 110,
    price_btn: "$120.00",

    imageArray: [
      {
        original: LightBoard,
        thumbnail: LightBoard,
      },
      ,
      {
        original: Men,
        thumbnail: Men,
      },
      {
        original: Women,
        thumbnail: Women,
      },
    ],
  },
  {
    id: 2,
    qty: 1,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis.",
    sale: "sale",
    image: Men,
    sku: "003",
    categories: "Canoening",
    tags: "Board,Surf,Surifing",
    remove: "Remove",
    button: "ADD TO CART",
    para: "SACUBA DIVING WETSUIT",
    equipment: "Equipment,Board",
    price: 120,
    price_btn: "$130.00",
    imageArray: [
      {
        original: paddleonly,
        thumbnail: paddleonly,
      },
      ,
      {
        original: Kayak,
        thumbnail: Kayak,
      },
      {
        original: Women,
        thumbnail: Women,
      },
    ],
  },
  {
    id: 3,
    qty: 1,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis.",
    sale: "sale",
    image: Women,
    sku: "003",
    categories: "equipment",
    tags: "Board,Surf,Surifing",
    remove: "Remove",
    button: "ADD TO CART",
    para: "Women's WetSuit Pro",
    equipment: "Equipment,Board",
    price: 130,
    price_btn: "$140.00",
    imageArray: [
      {
        original: LightBoard,
        thumbnail: LightBoard,
      },
      ,
      {
        original: paddleonly,
        thumbnail: paddleonly,
      },
      {
        original: Women,
        thumbnail: Women,
      },
    ],
  },
  {
    id: 4,
    qty: 1,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis.",
    sale: "sale",
    image: paddleonly,
    sku: "003",
    categories: "padding",
    tags: "Board,Surf,Surifing",
    remove: "Remove",
    button: "ADD TO CART",
    para: "Board paddle Pair",
    equipment: "Canoenig, Equipment",
    price: 150,
    price_btn: "$160.00",
    imageArray: [
      {
        original: paddle,
        thumbnail: paddle,
      },
      ,
      {
        original: Kayak,
        thumbnail: Kayak,
      },
      {
        original: paddleonly,
        thumbnail: paddleonly,
      },
    ],
  },
  {
    id: 5,
    qty: 1,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis.",
    sale: "sale",
    image: Kayak,
    sku: "003",
    categories: "rental",
    tags: "Board,Surf,Surifing",
    remove: "Remove",
    button: "ADD TO CART",
    para: "Kayak with Paddle",
    equipment: "Canoenig, Paddling",
    price: 160,
    price_btn: "$170.00",
    imageArray: [
      {
        original: paddle,
        thumbnail: paddle,
      },
      ,
      {
        original: LightBoard,
        thumbnail: LightBoard,
      },
      {
        original: Kayak,
        thumbnail: Kayak,
      },
    ],
  },
  {
    id: 6,
    qty: 1,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis.",
    sale: "sale",
    image: paddle,
    sku: "003",
    categories: "scubadiving",
    tags: "Board,Surf,Surifing",
    remove: "Remove",
    button: "ADD TO CART",
    para: "LIGHTWAVE BOARD",
    equipment: "Equipment,Board",
    price: 170,
    price_btn: "$180.00",
    imageArray: [
      {
        original: Kayak,
        thumbnail: Kayak,
      },
      ,
      {
        original: Men,
        thumbnail: Men,
      },
      {
        original: Women,
        thumbnail: Women,
      },
    ],
  },
];
export const relatedProduct = [
  { smallBoard: LightBoard, title: "TUNDER BOARD", rating: 4.5 },
  { smallBoard: LightBoard, title: "SHORT SURFBOARD FOAM", rating: 3 },
  { smallBoard: LightBoard, title: "MADMAX BOARD", rating: 4 },
];
export const billTotoal = [{}];
export const objCategories = [
  { name: "Board" },
  { name: "Canoening" },
  { name: "Equipment" },
  { name: "Padding" },
  { name: "Rental" },
  { name: "Scubadiving" },
  { name: "Surfing" },
];

export const instaGram = [
  { image: sliderone },
  { image: slidertwo },
  { image: sliderthree },
  { image: sliderfour },
];

export const navItems = [
  { name: "BLOG", path: "/blog" },
  { name: "SEARCH", path: "/contact" },
];

export const tablecells = [
  { cellheading: "Image" },
  { cellheading: "Product Name" },
  { cellheading: "Qty" },
  { cellheading: "Delete" },
  { cellheading: "SubTotal" },
];
function CreateValue(name, calories) {
  return { name, calories };
}
export const textSlid = [
  {
    heading: "DESCRIPTION",
    state: 0,
    paragraph:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis, varius turpis condimentum, venenatisnisi. Integer in sem a tortor semper fermentum. Donec at mi ipsum.",
  },
  {
    heading: "ADDITIONAL INFORMATION",
    state: 1,

    paragraph:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis, varius turpis condimentum, venenatisnisi. Integer in sem a tortor semper fermentum. Donec at mi ipsum.",
  },
  {
    heading: "REVIEWS (1)",
    state: 2,
    paragraph:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  venenatis, venenatisnisi. Integer in sem a tortor semper fermentum. Donec at mi ipsum.",
  },
];
export const footerData = [
  {
    heading: "About",
    paragraph: [
      "Bloowatch is speciallized software for waterssports schools (surfing,kitsurfing,salling,and diving )and outdoor activity providers skiing ,climbing",
    ],
  },
  {
    heading: "Content",
    paragraph: ["156-677-124-442-2887", "wave@bloowatch.com", "spain"],
  },
  { heading: "USEFULL LINK", paragraph: ["About us", "History", "Contact us"] },
];

export const detailArray = [
  {
    id: 1,
    qty: 1,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis.",
    sale: "sale",
    image: LightBoard,
    sku: "003",
    categories: "board",
    tags: "Board,Surf,Surifing",
    remove: "Remove",
    button: "ADD TO CART",
    para: "LIGHTWAVE BOARD",
    equipment: "Equipment,Board",
    price: 110,
    price_btn: "$120.00",

    imageArray: [
      {
        original: LightBoard,
        thumbnail: LightBoard,
      },
      ,
      {
        original: Men,
        thumbnail: Men,
      },
      {
        original: Women,
        thumbnail: Women,
      },
    ],
  },
  {
    id: 2,
    qty: 1,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis.",
    sale: "sale",
    image: Men,
    sku: "003",
    categories: "Canoening",
    tags: "Board,Surf,Surifing",
    remove: "Remove",
    button: "ADD TO CART",
    para: "SACUBA DIVING WETSUIT",
    equipment: "Equipment,Board",
    price: 120,
    price_btn: "$130.00",
    imageArray: [
      {
        original: paddleonly,
        thumbnail: paddleonly,
      },
      ,
      {
        original: Kayak,
        thumbnail: Kayak,
      },
      {
        original: Women,
        thumbnail: Women,
      },
    ],
  },
  {
    id: 6,
    qty: 1,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Maecenas porta lacus in odio varius, ut vehicula ipsum mattis.Aenean a risus venenatis.",
    sale: "sale",
    image: paddle,
    sku: "003",
    categories: "scubadiving",
    tags: "Board,Surf,Surifing",
    remove: "Remove",
    button: "ADD TO CART",
    para: "LIGHTWAVE BOARD",
    equipment: "Equipment,Board",
    price: 170,
    price_btn: "$180.00",
    imageArray: [
      {
        original: Kayak,
        thumbnail: Kayak,
      },
      ,
      {
        original: Men,
        thumbnail: Men,
      },
      {
        original: Women,
        thumbnail: Women,
      },
    ],
  },
];
