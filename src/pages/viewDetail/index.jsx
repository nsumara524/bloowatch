import React, { useState, useContext } from "react";
import {
  MyBox,
  Image,
  CustomBox,
  ButtonGroup,
  ButtonGroupone,
  MyGrid,
  Paragraph,
  GalleryGrid,
  Categoriesbox,
  MyButton,
} from "./styled-component";
import Header from "../../component/header/index";
import Footer from "../../component/footer/footer";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";
import "./index.css";
import { useLocation } from "react-router-dom";
import PropTypes from "prop-types";
import { DataContext } from "../../contexts/contextData";
import {
  Grid,
  Typography,
  Button,
  Box,
  Tabs,
  Tab,
  Container,
} from "@mui/material";

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
const Index = () => {
  const { cart, texttitleSlid, detailArray } = useContext(DataContext);
  const [value, setValue] = React.useState(0);
  const location = useLocation();
  const [productDetail, setProductDetail] = useState(location.state);

  console.log("abcdefgh", location.state);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  console.log("data from surfung", productDetail);
  const addProduct = (obj) => {
    setProductDetail((cart) => [...cart, obj]);
    console.log("cartItem ", cart);
  };

  return (
    <>
      <Header />
      <MyBox className="tunder-short">TUNDER SHORT BOARD</MyBox>
      <Container>
        <GalleryGrid container>
          <div className="Item-heights">
            <Grid container>
              <Grid item md={6} xs={12} sx={{ marginTop: "70px" }}>
                <ImageGallery
                  items={productDetail.imageArray}
                  thumbnailPosition="left"
                  disableSwipe={false}
                  disableThumbnailSwipe={false}
                  showNav={false}
                />
              </Grid>
              <Grid
                className="slider-grid"
                sx={{ px: 4, marginTop: "80px", textAlign: "left" }}
                item
                md={6}
                xs={12}
              >
                <Typography
                  style={{ fontSize: "20px", fontWeight: 600 }}
                  variant="h6"
                >
                  {productDetail.para}
                </Typography>
                <ButtonGroup style={{ display: "flex" }}>
                  <Button className="gallery-price-btn">
                    {productDetail.price_btn}
                  </Button>
                  <Button className="gallery-price-button">
                    {productDetail.price}
                  </Button>
                </ButtonGroup>
                <Typography
                  style={{ fontSize: "15px", marginTop: "10px" }}
                  variant="h6"
                >
                  {productDetail.description}
                </Typography>
                <Box style={{ display: "flex", marginTop: "10px" }}>
                  <MyButton onClick={() => addProduct(productDetail)}>
                    {productDetail.button}
                  </MyButton>
                </Box>
                <Categoriesbox>
                  <Typography className="sku" variant="h6">
                    SKU:{productDetail.sku}
                  </Typography>
                  <Typography className="sku" variant="h6">
                    CATEGORIES:{productDetail.categories}
                  </Typography>
                  <Typography className="sku" variant="h6">
                    TAGS:{productDetail.tags}
                  </Typography>
                </Categoriesbox>
              </Grid>
            </Grid>
          </div>
        </GalleryGrid>
        <MyGrid container className="text-slider">
          <Grid container>
            <Grid
              item
              md={12}
              xs={12}
              sx={{ marginTop: "40px", width: "100%" }}
            >
              <Box>
                <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                  <Tabs
                    value={value}
                    onChange={handleChange}
                    aria-label="basic tabs example"
                    variant="scrollable"
                  >
                    {texttitleSlid.map((obj) => (
                      <Tab label={obj.heading} {...a11yProps(obj.state)} />
                    ))}
                  </Tabs>
                </Box>
                {texttitleSlid.map((obj) => (
                  <TabPanel
                    style={{ textAlign: "left" }}
                    value={value}
                    index={obj.state}
                  >
                    {obj.paragraph}
                  </TabPanel>
                ))}
              </Box>
            </Grid>
          </Grid>
        </MyGrid>
        <MyGrid container>
          <div className="description-grid">
            <Grid container>
              {detailArray.map((obj) => (
                <Grid item md={4} sm={12}>
                  <>
                    <Paragraph className="sale">
                      <Typography className="sale-title">{obj.sale}</Typography>
                    </Paragraph>
                    <Image
                      sx={{ width: "75%", height: "242px" }}
                      src={obj.image}
                      alt="women wet suit"
                    />
                    <CustomBox>
                      <Button className="btns">{obj.button}</Button>
                      <Typography className="lighttwave-board" variant="h5">
                        {obj.para}
                      </Typography>
                      <Typography className="equipment-link" variant="h6">
                        {obj.equipment}
                      </Typography>
                      <ButtonGroupone
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        <Button className="price-btn">{obj.price}</Button>
                        <Button className="price-button">
                          {obj.price_btn}
                        </Button>
                      </ButtonGroupone>
                    </CustomBox>
                  </>
                </Grid>
              ))}
            </Grid>
          </div>
        </MyGrid>
      </Container>
      <Grid className="footer-class">
        <Footer />
      </Grid>
    </>
  );
};

export default Index;
