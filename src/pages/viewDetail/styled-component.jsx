import styled from "styled-components";
export const MyBox = styled.div`
  background: darkblue;
  color: white;
  font-size: 30px;
  padding-left: 120px;
  max-width: 100%;
  text-align: left;
  padding-top: 70px;
  height: 100px;
`;

export const GalleryGrid = styled.div`
  display: flex;
  margin-bottom: 40px;
  justify-content: center;
  // padding-left: 80px;
  // padding-right: 40px;
  height: 500px;
  // .Item-heights {
  //   width: 1000px;
  //   height: 500px;
  // }
`;

export const Image = styled.img`
    margin-top: 60px;
    width: 257px;
    height: 390px;
    align: center;
    }
  
`;
export const MyButton = styled.button`
  font-size: 10px;
  background: darkblue;
  color: white;
  border: none;
  padding-left: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-right: 20px;
  float: left;
  border-radius: 5px;
`;
export const Categoriesbox = styled.div`
  margin-top: 20px;

  .sku {
    font-size: 15px;
    font-weight: 600;
  }
`;

export const CustomBox = styled.div`
  .btns {
    background: darkblue;
    font-size: 15px;
    color: white;
    margin-top: 17px;
    conten: center;
    padding: 8px 51px 8px 51px;
  }
  .btns:hover {
    background: darkblue;
  }
  .equipment-link {
    font-size: 15px;
    color: blue;
  }
  .lighttwave-board {
    margin-top: 32px;
    font-size: 15px;
    font-weight: 600;
  }
`;
export const ButtonGroupone = styled.div`
  .price-btn {
    background: darkblue;
    font-size: 11px;
    color: white;
    margin-left: 20px;
    align-items: center;
    margin-top: 37px;
    padding: 0px 18px 0px 18px;
  }
  .price-btn:hover {
    background: darkblue;
  }
  .price-button {
    padding-top: 39px;
    font-size: 11px;
    text-decoration: line-through;
    color: black;
  }
`;
export const ButtonGroup = styled.div`
  .price-btn {
    background: darkblue;
    font-size: 11px;
    color: white;
    margin-left: 20px;
    align-items: center;
    margin-top: 37px;
    padding: 0px 18px 0px 18px;
  }
  .price-btn:hover {
    background: darkblue;
  }

  .price-button {
    padding-top: 39px;
    font-size: 11px;
    text-decoration: line-through;
    color: black;
  }

  .gallery-price-btn {
    font-size: 10px;
    background: darkblue;
    color: white;
    padding-left: 10px;
    margin-top: 10px;
    padding-right: 10px;
    float: left;
  }
  .gallery-price-btn:hover {
    background: darkblue;
  }
`;
export const MyGrid = styled.div`
  display: flex;
  justify-content: center;
  .description-grid {
    // height: 400px;
    width: 800px;
  }
`;
export const Paragraph = styled.p`
  color: white;
  background-color: darkblue;
  padding: 15px;
  width: 20px;
  height: 20px;
  font-size: 11px;
  text-align: center;
  border-radius: 500px;
  transform: translate(30px, 104px);
  .sale-title {
    font-size: 12px;
  }
`;
