import React, { useContext } from "react";
import Header from "../../component/header/index";
import Surfing from "../../component/surfing";
import Footer from "../../component/footer/footer";
import { DataContext } from "../../contexts/contextData";
const Dashboard = () => {
  return (
    <>
      <Header />
      <Surfing />
      <Footer />
    </>
  );
};

export default Dashboard;
