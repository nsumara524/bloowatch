import styled from "styled-components";
export const MyBox = styled.div`
  background: darkblue;
  color: white;
  font-size: 30px;
  padding-left: 120px;
  max-width: 100%;
  text-align: left;
  padding-top: 70px;
  height: 100px;
`;
export const Mycontainer = styled.div`
  .proceed-button {
    background-color: darkblue;
    color: white;
    padding-left: 30px;
    padding-right: 30px;
  }
  .proceed-button:hover {
    background: darkblue;
  }
`;
export const Grid = styled.div`
  display: flex;
  justify-content: center;
  .grid-center {
    margin-top: 80px;
    height: 400;
    max-width: 800px;
    width: 100%;
  }
`;
export const Mybox = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 54px;
  .apply-coupon {
    background-color: darkblue;
    color: white;
    padding-left: 30px;
    padding-right: 30px;
    margin-right: 10px;
  }
  .apply-coupon:hover {
    background-color: darkblue;
  }
  .updata-cart {
    background-color: darkblue;
    color: white;
    padding-left: 30px;
    padding-right: 30px;
  }
  .updata-cart:hover {
    background-color: darkblue;
  }
`;
