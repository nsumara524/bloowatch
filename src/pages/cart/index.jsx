import React, { useContext, useEffect, useState } from "react";
import { Button, Container } from "@mui/material";
import { MyBox, Mycontainer, Grid, Mybox } from "./styled-component";
import Header from "../../component/header/index";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import Footer from "../../component/footer/footer";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import ClearIcon from "@mui/icons-material/Clear";
import "./style.css";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import { DataContext } from "../../contexts/contextData";
const Index = () => {
  const { cart, setCart, billcartTotoal, tablecellsdata } =
    useContext(DataContext);
  function handelDelete(id) {
    const filtered = cart.filter((data, index) => data !== id);
    setCart(filtered);
  }
  const [totoalprice, setTotalPrice] = useState();
  const handleIncrement = (row, index) => {
    console.log("row", row);
    let arrayData = cart;
    let data = row.qty + 1;
    arrayData[index].qty = data;
    setCart([...arrayData]);
    console.log("data add");
  };
  const handleDecrement = (row, index) => {
    console.log("row", row);
    let arrayData = cart;
    let data = row.qty - 1;
    arrayData[index].qty = data;
    console.log("arrayData", cart);
    setCart([...arrayData]);
  };
  useEffect(() => {
    let sum = 0;
    for (let i = 0; i < cart.length; i++) {
      // console.log("hlo i am form for loop ", cart[i].price, cart[i].qty);
      console.log("hlo i am price Calculate ", cart[i].price * cart[i].qty);
      sum = sum + cart[i].price * cart[i].qty;
    }
    console.log("price Sum ", sum);
    setTotalPrice(sum);
  });
  return (
    <>
      <Header />
      <MyBox>CART</MyBox>
      <Container>
        <Grid container>
          <div className="grid-center">
            <TableContainer component={Paper}>
              <Table
                sx={{ minWidth: 650 }}
                size="small"
                aria-label="a dense table"
              >
                <TableHead>
                  <TableRow>
                    {tablecellsdata.map((obj) => (
                      <TableCell align="center">{obj.cellheading}</TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {cart.map((row, index) => (
                    <TableRow
                      key={row.name}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell>
                        <img
                          style={{ height: "60px", width: "60px" }}
                          src={row.image}
                          alt="board"
                        />
                      </TableCell>
                      <TableCell align="center">{row.para}</TableCell>

                      <TableCell align="right">
                        <Button
                          startIcon={
                            <ArrowBackIosIcon style={{ fontSize: "17px" }} />
                          }
                          disabled={row.qty === 1 ? true : false}
                          onClick={() => handleDecrement(row, index)}
                        ></Button>
                        {row.qty}
                        <Button
                          startIcon={
                            <ArrowForwardIosIcon style={{ fontSize: "17px" }} />
                          }
                          onClick={() => handleIncrement(row, index)}
                        ></Button>
                      </TableCell>
                      <TableCell align="right">
                        <ClearIcon
                          style={{ cursor: "pointer" }}
                          onClick={() => handelDelete(row)}
                        />
                      </TableCell>
                      <TableCell align="right">{row.qty * row.price}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </Grid>
        <Mybox>
          <Button className="apply-coupon">APPLY COUPON</Button>
          <Button className="updata-cart">UPDATE CART</Button>
        </Mybox>
        <div
          className="price-count-grid"
          style={{
            marginLeft: "27px",
            marginTop: "54px",
            height: "383px",
            maxWidth: "823px",
            width: "87%",
          }}
        >
          <TableContainer component={Paper}>
            <Table
              sx={{ minWidth: 650 }}
              size="small"
              aria-label="a dense table"
            >
              <TableBody>
                <TableRow>
                  <TableCell align="left">SUBTOTAL</TableCell>
                  <TableCell align="left">{totoalprice}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="left">SHOPPING</TableCell>
                  <TableCell align="left">
                    Enter your address to view your spping option
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="left">VALUE</TableCell>
                  <TableCell align="left">{totoalprice}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <Mycontainer style={{ marginTop: "30px" }}>
            <Button className="proceed-button">Proceed to Checkout</Button>
          </Mycontainer>
        </div>
      </Container>
      <Footer />
    </>
  );
};

export default Index;
